* `rewrite-configure.lua`: Replace comment autoconf shell patterns with
  fixed versions.  Written in Lua for potential future integration into
  `redhat-rpm-config`.
